﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControler : MonoBehaviour {

public float maxSpeed = 1f;
public float speed = 1f;

public float enterXTop = 0f;
public float enterYTop = 16.5f;
public float enterXRight = -10f;
public float enterYRight = 1f;
public float enterXLeft = 10f;
public float enterYLeft =1f;

private Animator anim;
private bool alive = true;
private Rigidbody2D rgb2D;


	void Start () {
		rgb2D = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
	}
	void OnCollisionEnter2D(Collision2D collision){
	if (collision.gameObject.tag == "Bulled"){
			//Debug.Log("ME MUREO");
			
			Destroy(gameObject);
        }
	}
	
	void FixedUpdate () {
		if(!alive){
            Destroy(gameObject);
        }
		rgb2D.AddForce(Vector2.right * speed);
		float limitedSpeed = Mathf.Clamp(rgb2D.velocity.x, -maxSpeed, maxSpeed);
		rgb2D.velocity = new Vector2(limitedSpeed, rgb2D.velocity.y);

		if(rgb2D.velocity.x > -0.01f && rgb2D.velocity.x < 0.01f){
			speed = -speed;
			rgb2D.velocity = new Vector2(speed, rgb2D.velocity.y);
		}


		anim.SetBool("vivo", alive);

		if( rgb2D.velocity.x > 0.1f){
            transform.localScale = new Vector3 (3f, 3f, 1f);
        }
        if ( rgb2D.velocity.x < -0.1f){
            transform.localScale = new Vector3 (-3f, 3f, 1f);
        }
	}

	void OnTriggerEnter2D(Collider2D collision){ 
        if (collision.gameObject.tag == "TeletransportTop"){
			transform.position = new Vector3(enterXTop,enterYTop + 10f,0f);
        }   
		if (collision.gameObject.tag == "TeletransportLeft"){
			transform.position = new Vector3(enterXRight -2f,enterYRight,0f);
        }   
		if (collision.gameObject.tag == "TeletransportRight"){
			transform.position = new Vector3(enterXLeft + 1f,enterYLeft,0f);
        }		 
    } 
	public void setAlive(bool dead){
        if(dead){
            alive = false;
        } 
    }

	
	
}