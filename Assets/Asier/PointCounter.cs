﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PointCounter : MonoBehaviour
{

    public static float points = 0;
    Text scoreText;
    
        void Start()
    {
        scoreText = GetComponent<Text> ();
    }
    void Update()
    {
        scoreText.text = "Score: " + points;
    }
}
