﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SapwnEnemys : MonoBehaviour {

public GameObject enemyPrefab;
public int totalEnemy = 0;
public int maxEnemy = 10;

public float generatorTimer = 5f;

	void Start () {
		InvokeRepeating("createEnemy",0f,generatorTimer);
	}
	
	void Update () {
		
	}

	void createEnemy(){
		Instantiate(enemyPrefab, transform.position, Quaternion.identity);
	}

	public void StartGenerator(){
		InvokeRepeating("createEnemy",0f,generatorTimer);
	}
	public void EndGenerator(){
		CancelInvoke("createEnemy");
	}
}
