﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPScript : MonoBehaviour
{
    public static int hpValue;
    public Text hp;

    public GameObject Player;
    Player hpPlayer;
    // Start is called before the first frame update
    void Start()
    {
        hpPlayer = Player.GetComponent<Player>();
        hpValue = hpPlayer.hp;
        hp = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        hp.text = "HP: " + hpValue;
    }
}
