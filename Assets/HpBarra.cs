﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpBarra : MonoBehaviour
{
    public GameObject Player;
    Player hpPlayer;
    public Scrollbar HealtBarr;
    private float hpTotal;
    
    void Start()
    {
        hpPlayer = Player.GetComponent<Player>();
    } 

    void Update(){
        hpTotal = hpPlayer.hp;
        HealtBarr.size = hpTotal/100f;
    }
}
