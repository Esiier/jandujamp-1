﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private float dir;

    GameObject thePlayer;

    Player playerScript;

    
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D> ();
        thePlayer = GameObject.FindGameObjectWithTag("Player");
        playerScript = thePlayer.GetComponent<Player>();
        if(playerScript.Direction == 1){
            rb2d.velocity = new Vector2(0, 0);
        }
        else if(playerScript.Direction == 0){
            rb2d.velocity = new Vector2(0, 0);
        }
        
    }
    void Update()
    {
        
    }
    void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.tag == "Enemy"){
            //EnemyControler.setAlive(false);
            ScoreScript.scoreValue += 10;
            Destroy(gameObject);            
        }
    } 
}
