﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpwan : MonoBehaviour {

public GameObject pistolPrefab;

//public GameObject shotPrefab;

//public GameObject minePrefab;
//public int totalEnemy = 0;
//public int maxEnemy = 10;

public float generatorTimer = 15f;

	void Start () {
		//InvokeRepeating("createEnemy",0f,generatorTimer);
	}
	
	void Update () {
		
	}

	void createEnemy(){
		Instantiate(pistolPrefab, transform.position, Quaternion.identity);
	}

	public void StartGenerator(){
		InvokeRepeating("createEnemy",0f,generatorTimer);
	}
	public void EndGenerator(){
		CancelInvoke("createEnemy");
	}
}
