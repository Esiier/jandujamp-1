﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineGun : MonoBehaviour
{
    public bool alive;

    // Start is called before the first frame update
    void Start()
    {
        alive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(!alive){
            Destroy(gameObject);
        }
        
    }
    public void setAlive(bool dead){
        if(dead){
            alive = false;
        } 
    }
}
