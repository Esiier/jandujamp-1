﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulled : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private float dir;

    GameObject thePlayer;

    Player playerScript;

    
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D> ();
        thePlayer = GameObject.FindGameObjectWithTag("Player");
        playerScript = thePlayer.GetComponent<Player>();
        if(playerScript.Direction == 1){
            rb2d.velocity = new Vector2(10f, 0);
            transform.localScale = new Vector3 (2f, 2f, 1f);
        }
        else if(playerScript.Direction == 0){
            rb2d.velocity = new Vector2(-10f, 0);
            transform.localScale = new Vector3 (-2f, 2f, 1f);
        }
        
    }
    void Update()
    {
        
    }
    void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.tag == "Terra"){
            Destroy(gameObject);            
        }
        if (collision.gameObject.tag == "Gun"){
            Destroy(gameObject);            
        }
        if (collision.gameObject.tag == "Bulled"){
            Destroy(gameObject);            
        }
         if (collision.gameObject.tag == "Enemy"){
            ScoreScript.scoreValue += 10;
            Destroy(gameObject);            
        }
    } 
}
