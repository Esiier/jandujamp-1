﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour
{
    public enum  DirectionBulled {NONE, LEFT, RIGHT};      
    DirectionBulled Dir;

    public int Direction;    
    private Rigidbody2D rb2d;
    Object bulledRef;

    Object mineRef;
    private float speed;
    private float jumpSpeed;
    private bool canJump;
    
    private bool haveGun;

    private bool haveShotGun;

    private bool haveMineGun;
    private float DirectionX;

    public GameObject pistol;

    public GameObject shotgun;

     public GameObject minegun;

    public int ammo;

    //cosas de la animacion no tocar Arnau.

    private Animator anim;

    public float enterXTop = 0f;
    public float enterYTop = 16.5f;
    public float enterXRight = -10f;
    public float enterYRight = 1f;
    public float enterXLeft = 10f;
    public float enterYLeft =1f;

    public int hp;

    //public Text scoreText;
    
    void Start()
    {
        hp = 3;
        ammo = 0;
        bulledRef = Resources.Load("Bulled");
        mineRef = Resources.Load("MineBulled");
        speed = 5f;
        jumpSpeed = 8f;
        rb2d = GetComponent<Rigidbody2D> ();
        anim = GetComponent<Animator> ();
    }

   
    void FixedUpdate()
    {
        if(hp <=0){
            SceneManager.LoadScene("MainMenu");
        }
        if(DirectionX > 0){
            Dir = DirectionBulled.RIGHT;
            Direction = 1;
        }
        else if(DirectionX < 0){
            Dir = DirectionBulled.LEFT; 
            Direction = 0;           
        }        
        float moveHorizontal = Input.GetAxis ("Horizontal");
        DirectionX = moveHorizontal;
        rb2d.velocity = new Vector2 (moveHorizontal * speed, rb2d.velocity.y);

        
        if(Input.GetKeyDown("space") && canJump){
            Debug.Log("JUMP");
            Jump();
        }
        if(Input.GetKeyDown("e")){
            if(haveGun){
                Shoot();
            }
            else if(haveShotGun){
                Shooto();
            }
            else if(haveMineGun){
                Mine();
            }
        }

        //cosas de la animacion no lo toques arnau

        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));
        anim.SetBool("Ground", canJump);

        if( moveHorizontal > 0.1f){
            transform.localScale = new Vector3 (4f, 4f, 1f);
        }
        if ( moveHorizontal < -0.1f){
            transform.localScale = new Vector3 (-4f, 4f, 1f);
        }
         
    }
    void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.tag == "Terra"){
            canJump = true;            
        }
        else if(collision.gameObject.tag == "Gun"){
            
            if(collision.gameObject.name == "gun" || collision.gameObject.name == "gun(Clone)"){
                ammo = 10;
                collision.gameObject.GetComponent<Weapon>().setAlive(true);
                pistol.SetActive(true);
                shotgun.SetActive(false);
                minegun.SetActive(false);
                haveGun = true;
                haveShotGun = false;
                haveMineGun = false;
            }
            if(collision.gameObject.name == "ShotGun" || collision.gameObject.name == "ShotGun(Clone)"){
                ammo = 15;
                collision.gameObject.GetComponent<ShotGun>().setAlive(true);
                shotgun.SetActive(true);
                pistol.SetActive(false);
                minegun.SetActive(false);
                haveShotGun = true;
                haveGun = false;
                haveMineGun = false;
            }
            if(collision.gameObject.name == "mine" || collision.gameObject.name == "mine(Clone)"){
                ammo = 5;
                collision.gameObject.GetComponent<MineGun>().setAlive(true);
                minegun.SetActive(true);
                pistol.SetActive(false);
                shotgun.SetActive(false);               
                haveMineGun = true;
                haveShotGun = false;
                haveGun = false;
            }
        }
        else if(collision.gameObject.tag == "Enemy"){
            hp--;
            HPScript.hpValue -= 1;
            Debug.Log(hp);
        }
    }

    void Jump(){
        canJump = false;
        //rb2d.AddForce(Vector2.up * speed);
        rb2d.velocity = new Vector2(0, +jumpSpeed);
    }
    void Shoot(){
        GameObject Bulled = (GameObject)Instantiate(bulledRef);
        
        if(ammo > 0){
            ammo--;
            if(Dir == DirectionBulled.RIGHT){
                Bulled.transform.position = new Vector3 (transform.position.x + .4f, transform.position.y + .2f, -1);
            }
            else if(Dir == DirectionBulled.LEFT){
                Bulled.transform.position = new Vector3 (transform.position.x - .4f, transform.position.y + .2f, -1);
            }
            else{
                Bulled.transform.position = new Vector3 (transform.position.x + .4f, transform.position.y + .2f, -1);
            }
        }
        else{
           haveGun = false;
            pistol.SetActive(false);
        }
        //Instantiate(bulledPrefab, new Vector2(Player.x,0), Quaternion.identity);
    }
    void Shooto(){
        GameObject Bulled = (GameObject)Instantiate(bulledRef);
        GameObject Bulledo = (GameObject)Instantiate(bulledRef);
        //GameObject Bulledoo = (GameObject)Instantiate(bulledRef);
        if(ammo > 0){
            ammo--;
            if(Dir == DirectionBulled.RIGHT){
                Bulled.transform.position = new Vector3 (transform.position.x + .4f, transform.position.y + .4f, -1);
                Bulledo.transform.position = new Vector3 (transform.position.x + .4f, transform.position.y + .0f, -1);
                
            }
            else if(Dir == DirectionBulled.LEFT){
                Bulled.transform.position = new Vector3 (transform.position.x - .4f, transform.position.y + .4f, -1);
                Bulledo.transform.position = new Vector3 (transform.position.x - .4f, transform.position.y + .0f, -1);
                
            }
            else{
                Bulled.transform.position = new Vector3 (transform.position.x + .4f, transform.position.y + .4f, -1);
                Bulledo.transform.position = new Vector3 (transform.position.x + .4f, transform.position.y + .0f, -1);
                
            }
        }
        else{
            haveShotGun = false;
            shotgun.SetActive(false);
        }
    }

    void Mine(){
        GameObject Mine = (GameObject)Instantiate(mineRef);
        
        if(ammo > 0){
            ammo--;
            if(Dir == DirectionBulled.RIGHT){
                Mine.transform.position = new Vector3 (transform.position.x + .4f, transform.position.y + .2f, -1);
            }
            else if(Dir == DirectionBulled.LEFT){
                Mine.transform.position = new Vector3 (transform.position.x - .4f, transform.position.y + .2f, -1);
            }
            else{
                Mine.transform.position = new Vector3 (transform.position.x + .4f, transform.position.y + .2f, -1);
            }
        }
        else{
            haveMineGun = false;
            minegun.SetActive(false);
        }
    }

    public int getDir(){
        return Direction;
    }

    void OnTriggerEnter2D(Collider2D collision){ 
        if (collision.gameObject.tag == "TeletransportTop"){
			transform.position = new Vector3(enterXTop,enterYTop,0f);
        }   
		if (collision.gameObject.tag == "TeletransportRight"){
			transform.position = new Vector3(enterXRight,enterYRight,0f);
        }   
		if (collision.gameObject.tag == "TeletransportLeft"){
			transform.position = new Vector3(enterXLeft,enterYLeft,0f);
        }   
    } 
}
