﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSpawn : MonoBehaviour {

public GameObject pistolPrefab;

public GameObject shotPrefab;

public GameObject minePrefab;
private int numbGun;
//public int totalEnemy = 0;
//public int maxEnemy = 10;

public float generatorTimer = 15f;

	void Start () {
		InvokeRepeating("createEnemy",0f,generatorTimer);
        
	}
	
	void Update () {
		
	}

	void createEnemy(){
        numbGun = Random.Range(0,3);
        if(numbGun == 0){
            Instantiate(pistolPrefab, transform.position, Quaternion.identity);
        }
        if(numbGun == 1){
            Instantiate(shotPrefab, transform.position, Quaternion.identity);
        }
        if(numbGun == 2){
            Instantiate(minePrefab, transform.position, Quaternion.identity);
        }
		
	}

	
}
